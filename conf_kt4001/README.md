![mama](http://download.litmis.com.s3.amazonaws.com/cdn/mama_logo.png)
# mama

This is litmis running mama started node applications. 

## /www/apachedft/conf/httpd.conf
I am using standard http apachedft (port 80).
This machine firewall had most ports locked out.
The node applications needed port 47700-47720.
To work around firewall, node code and config changed to use
127.0.0.1 for proxy/reverse (localhost not blocked).

Apache FastCGI requires QHTTPSVR.LIB/QZFAST.SRVPGM.
We are starting 'mama' from fastcgi, so we added a application type .mama for the fastcgi.conf.
```
LoadModule zend_enabler_module /QSYS.LIB/QHTTPSVR.LIB/QZFAST.SRVPGM

# mama fastcgi
AddType application/x-httpd-mama .mama
AddHandler fastcgi-script .mama
```

We add proxy and reverse proxy for our independent node web applications.
This 'forwarding' technique is well know for people use to starting node applications by hand.
However, mama will be starting all the node applications for us with STRTCPSVR.
```
# flight 400 rest api
ProxyPass /flight400/api http://127.0.0.1:47700/flight400/api/
ProxyPassReverse /flight400/api http://127.0.0.1:47700/flight400/api/

# zoo rest api
ProxyPass /zoo/api http://127.0.0.1:47710/zoo/api/
ProxyPassReverse /zoo/api http://127.0.0.1:47710/zoo/api/

# silly hats
ProxyPass /silly http://127.0.0.1:47720/silly
ProxyPassReverse /silly http://127.0.0.1:47720/silly
```

## /www/apachedft/conf/fastcgi.conf
Our factcgi configuration match to Apache starts only application type mama (x-httpd-mama).
We installed mama in /QOpenSys/usr/bin/mama.
```
; Static DB2 servers
Server type="application/x-httpd-mama" CommandLine="/QOpenSys/usr/bin/mama" StartProcesses="1" SetEnv="MAMA=/www/apachedft/conf/mama.conf"

; Where to place socket files
IpcDir /www/apachedft/logs
```

## /www/apachedft/conf/mama.conf
We have 3 node applications started by mama. 
These applications were developed in a chroot (/QOpenSys/node6),
but we are running from root with Apache. Here again we only need
assure *PUBLIC has read/execute access to these node applications.
```
# START=   - reserved word application
# CD=      - reserved word change directory before start
# MAIN=    - reserved word main executable to start
# PARM=    - reserved word parm to main (many)
# EXECUTE= - reserved word start application
# PATH=    - PASE PATH env var
# LIBPATH= - PASE LIBPATH env var
# ANY=     - PASE any env var
#
# Any console kills program (do not use)
# console.log('Magic happens on port ' + port);
#
START=flight400
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/JESSEG/litmis-nodejs/examples/express_api_flight400_node6/server.js
EXECUTE=flight400
START=zoo
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/JESSEG/litmis-nodejs/examples/express_api_bears_node6/server.js
EXECUTE=zoo
START=silly
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/JESSEG/litmis-nodejs/examples/express_silly_hats_node6/app.js
EXECUTE=silly
```

## application specific (not mama related)
These node applications have hybrid web resource serving. That is,
not everything is server from node application. My 'best practice'
suggestion is to split out the things Apache can serve without node help,
using symbolic links, simply avoid overhead of running thorough proxies.
```
bash-4.3$ cd /www/apachedft/htdocs

bash-4.3$ ls -l
total 32
lrwxrwxrwx    1 jesseg   0               136 Aug 15 18:12 flight400 -> /home/JESSEG/litmis-nodejs/examples/express_api_flight400_node6/view
lrwxrwxrwx    1 jesseg   0                66 Aug 16 10:25 index.html -> /home/JESSEG/mama_conf/index.html
lrwxrwxrwx    1 jesseg   0               134 Aug 15 18:11 silly_public -> /home/JESSEG/litmis-nodejs/examples/express_silly_hats_node6/public
lrwxrwxrwx    1 jesseg   0               136 Aug 15 18:12 zoo -> /home/JESSEG/litmis-nodejs/examples/express_api_bears_node6/app/view
```

* The 'silly' hats application is a traditional node, express, jade itoolkit db2 and RPG call application. However silly_public/ was linked to public/ resources,
allowing Apache direct access to resources like stylesheet and images.

* The 'zoo' bears application is modern express, json api db2 application (best practice). Apache bears/ was linked to all view/ resources,
allowing Apache direct access to resources like stylesheet, images, and JQuery GUI.

* The 'flight400' bears application is modern express, json api itoolkit RPG call application (best practice). Apache flight400/ was linked to all view/ resources,
allowing Apache direct access to resources like stylesheet, images, and JQuery GUI.


# XMLSERVICE
XMLSERVICE was compiled allowing *NONE uid/password. To protect our system from malicious use, we only allow 127.0.0.1 requests to XMLSERVICE.
As seen previously, we have node applications using 127.0.0.1, therefore will function from within machine (loopback).

```
# XML Toolkit http settings
# lock down xmlservice to 127.0.0.1 (internal only)
# xmlservice created with *NONE enabled
ScriptAlias /cgi-bin/ /QSYS.LIB/XMLSERVICE.LIB/
<Directory /QSYS.LIB/XMLSERVICE.LIB/>
  AllowOverride None
  SetHandler cgi-script
  Options +ExecCGI
  order deny,allow
  deny from all
  allow from 127.0.0.1
</Directory>
# End XML Toolkit http settings
```

