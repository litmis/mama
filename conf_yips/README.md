![mama](http://download.litmis.com.s3.amazonaws.com/cdn/mama_logo.png)
# mama

This is Yips machine running mama started node applications. 

## /www/zendsvr6/conf/httpd.conf
I am using standard Zend Server 6 http zendsvr6 (port 80).
This may be interesting to others, as many IBM i shops use Zend Server.

Apache FastCGI requires QHTTPSVR.LIB/QZFAST.SRVPGM. This is module shared with Zend for the Zend Server project (PHP).
We are starting 'mama' from fastcgi, so we added a application type .mama for the fastcgi.conf.
```
LoadModule zend_enabler_module /QSYS.LIB/QHTTPSVR.LIB/QZFAST.SRVPGM

# mama fastcgi
AddType application/x-httpd-mama .mama
AddHandler fastcgi-script .mama
```

We add proxy and reverse proxy for our independent node web applications.
This 'forwarding' technique is well know for people use to starting node applications by hand.
However, mama will be starting all the node applications for us with STRTCPSVR.
```
# flight 400 rest api
ProxyPass /flight400/api http://yips.idevcloud.com:47700/flight400/api/
ProxyPassReverse /flight400/api http://yips.idevcloud.com:47700/flight400/api/

# zoo rest api
ProxyPass /zoo/api http://yips.idevcloud.com:47710/zoo/api/
ProxyPassReverse /zoo/api http://yips.idevcloud.com:47710/zoo/api/

# silly hats
ProxyPass /silly http://yips.idevcloud.com:47720/silly
ProxyPassReverse /silly http://yips.idevcloud.com:47720/silly
```

##  /www/zendsvr6/conf/fastcgi.conf

Our factcgi configuration match to Apache starts both application type php (x-httpd-php), 
and application type mama (x-httpd-mama). We installed mama in PASE root machine /QOpenSys/usr/bin/mama.
Only need assure *PUBLIC has read/execute access to mama.
Notice type php application is not affected by the addition of type mama application.

```
; Static PHP servers for default user
Server type="application/x-httpd-php" CommandLine="/usr/local/zendsvr6/bin/php-cgi.bin" StartProcesses="1" SetEnv="LIBPATH=/usr/local/zendsvr6/lib" SetEnv="PHPRC=/usr/local/zendsvr6/etc/" SetEnv="PHP_FCGI_CHILDREN=10" SetEnv="PHP_FCGI_MAX_REQUESTS=0" ConnectionTimeout="30" RequestTimeout="60" SetEnv="CCSID=1208" SetEnv="LANG=C" SetEnv="INSTALLATION_UID=100313092601" SetEnv="LDR_CNTRL=MAXDATA=0x40000000" SetEnv="ZEND_TMPDIR=/usr/local/zendsvr6/tmp" SetEnv="TZ=<EST>5<EDT>,M3.2.0,M11.1.0"

Server type="application/x-httpd-mama" CommandLine="/QOpenSys/usr/bin/mama" StartProcesses="1" SetEnv="MAMA=/www/zendsvr6/conf/mama.conf"

; Where to place socket files
IpcDir /www/zendsvr6/logs```
```

## /www/zendsvr6/conf/mama.conf
We have 3 node applications started by mama. 
These applications were developed in my home root (/home/ADC),
and, we are running from root with Apache. We only need
assure *PUBLIC has read/execute access to these node applications.

```
# START=   - reserved word application
# CD=      - reserved word change directory before start
# MAIN=    - reserved word main executable to start
# PARM=    - reserved word parm to main (many)
# EXECUTE= - reserved word start application
# PATH=    - PASE PATH env var
# LIBPATH= - PASE LIBPATH env var
# ANY=     - PASE any env var
#
# Any console kills program (do not use)
# console.log('Magic happens on port ' + port);
#
START=flight400
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/ADC/nodejs_flight400/server.js
EXECUTE=flight400
START=zoo
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/ADC/node4_tutorial_bears/server.js
EXECUTE=zoo
START=silly
MAIN=/QOpenSys/QIBM/ProdData/OPS/Node6/bin/node
PARM=/home/ADC/MyIBMiNodeJs/app.js
EXECUTE=silly
```

## application specific (not mama related)
These node applications have hybrid web resource serving. That is,
not everything is server from node application. My 'best practice'
suggestion is to split out the things Apache can serve without node help,
using symbolic links, simply avoid overhead of running thorough proxies.

```
bash-4.3$ cd /www/zendsvr6/htdocs

bash-3.00$ ls -l | grep ADC
lrwxrwxrwx    1 adc      0                62 Jul 20 16:54 flight400 -> /home/ADC/nodejs_flight400/view
lrwxrwxrwx    1 adc      0                58 Jul 31 14:19 silly_public -> /home/ADC/MyIBMiNodeJs/public
lrwxrwxrwx    1 adc      0                78 Jul 28 17:29 zoo -> /home/ADC/node4_tutorial_bears/app/view
```

* The 'silly' hats application is a traditional node, express, jade itoolkit db2 and RPG call application. However silly_public/ was linked to public/ resources,
allowing Apache direct access to resources like stylesheet and images.

* The 'zoo' bears application is modern express, json api db2 application (best practice). Apache bears/ was linked to all view/ resources,
allowing Apache direct access to resources like stylesheet, images, and JQuery GUI.

* The 'flight400' bears application is modern express, json api itoolkit RPG call application (best practice). Apache flight400/ was linked to all view/ resources,
allowing Apache direct access to resources like stylesheet, images, and JQuery GUI.



