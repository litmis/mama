### gmake options (see make_libdb400.sh)
### - gmake -f Makefile
### gcc options (Makefile)
### -v            - verbose compile
### -Wl,-bnoquiet - verbose linker
### -shared       - shared object
### -maix64       - 64bit
### -isystem      - compile PASE system headers
### -nostdlib     - remove libgcc_s.a and crtcxa_s.o
FCGI        = ./fcgi-2.4.1-SNAP-0311112127
CC          = gcc
# CCFLAGS32   = -v verbose
CCFLAGS32   = -g -Wno-int-to-pointer-cast -Wno-pointer-to-int-cast
CCFLAGS64   = $(CCFLAGS32) -maix64 -DTGT64
AR          = ar
AROPT       = -X32_64
INCLUDEPATH = -isystem /QOpenSys/usr/include -I. -I$(FCGI)/include

### mama
LIBDEP400       = -L. -L/QOpenSys/usr/lib -nostdlib -lpthreads -lc -liconv -ldl -lpthread
mama32         = mama
mamaLIBOBJS32  = mama.o fcgiapp.o os_unix.o fcgi_stdio.o
mamaLIBDEPS32  = /QOpenSys/usr/lib/crt0.o $(LIBDEP400)

### global
CCFLAGS      = $(CCFLAGS32)
mama        = $(mama32)
mamaLIBOBJS = $(mamaLIBOBJS32)
mamaLIBDEPS = $(mamaLIBDEPS32)

### tells make all things to do (ordered)
all: cpy clean removeo $(mama) install

### PASE
### generic rules
### (note: .c.o compiles all c parts in OBJS list)
.SUFFIXES: .o .c
.c.o:
	$(CC) $(CCFLAGS) $(INCLUDEPATH) -c $<

### -- mama
$(mama32): $(mamaLIBOBJS)
	$(CC) $(CCFLAGS) $(mamaLIBOBJS) $(mamaLIBDEPS) -o $(mama)

### -- oh housekeeping?
cpy:
	tar -xf fcgi.tar
	cp $(FCGI)/libfcgi/fcgiapp.c .
	cp $(FCGI)/libfcgi/fcgiapp.c .
	cp $(FCGI)/libfcgi/os_unix.c .
	cp $(FCGI)/libfcgi/fcgi_stdio.c .
clean:
	rm -f $(mama)
removeo:
	rm -f *.o
install:
	cp ./mama /QOpenSys/usr/bin/.

