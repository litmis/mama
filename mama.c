#include "fcgi_stdio.h" /* fcgi library; put it first*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>

#define MAMA_VERSION "1.0.1"


#define MAMA_PAUSE 5
#define MAMA_MAX_ARGV 512
#define MAMA_MAX_STR 4096


static void mama_http_200(char *reason)
{
  printf("HTTP/1.1 200 Ok\r\n"
         "Content-type: application/json; charset=utf-8\r\n"
         "\r\n"
         "{\"version\":\"%s\",\"ok\":true,\"reason\":\"%s\"}", MAMA_VERSION, reason);
}

static void mama_http_403(char *reason)
{
  printf("HTTP/1.1 403 Forbidden\r\n"
         "Content-type: application/json; charset=utf-8\r\n"
         "\r\n"
         "{\"version\":\"%s\",\"ok\":true,\"reason\":\"%s\"}", MAMA_VERSION, reason);
}

static void mama_start(char * argvi[])
{
    int i = 0;
    int j = 0;
    int rc = 0;
    int status = 0;
    pid_t wait_pid = 0;
    pid_t child_pid = 0;
    char *filename = NULL;
    int argc = 0;
    char * argv[MAMA_MAX_ARGV];
    char * key = NULL;
    char * val = NULL;
    char * equal = NULL;
    FILE *fp = NULL;

    /* endless loop */
    while (1) {
      /* new process */
      child_pid = fork();
      /* child process */
      if (child_pid == 0) {
        memset(argv,0,sizeof(argv));
        for (i=0; argvi[i] && i<MAMA_MAX_ARGV; i++) {
          /* (setenv MAMA=/www/apachedft/conf/mama.conf)
           # START=   - reserved word application
           # PATH=    - PASE PATH env var
           # LIBPATH= - PASE LIBPATH env var
           # ANY=     - PASE any env var
           # MAIN=    - reserved word main executable to start
           # PARM=    - reserved word parm to main (many)
           # EXECUTE= - reserved word start application
           */
          equal = strchr(argvi[i],'=');
          if (equal) {
            key = argvi[i];
            val = equal + 1;
            // # ANY= - reserved word start application 
            if (strstr(key,"START=")) {
              // nothing to do
            }
            else if (strstr(key,"MAIN=")) {
              filename = val;
              argv[argc] = val;
              argc++;
            }
            else if (strstr(key,"PARM=")) {
              argv[argc] = val;
              argc++;
            }
            else if (strstr(key,"CD=")) {
              chdir(val);
            }
            else if (strstr(key,"EXECUTE=")) {
              rc = execv(filename, argv);
            // # ANY=     - PASE any env var
            } else {
              *equal = '\0';
              setenv(key,val,1); // change child env vars
              *equal = '=';
            }
          }
        }
        /* should never make it here */
        exit(-1);
      /* parent process (thread) */
      } else {
        /* wait for child to die */
        do {
          wait_pid = waitpid(child_pid, &status, WUNTRACED | WCONTINUED);
        } while (wait_pid != child_pid && !WIFEXITED(status) && !WIFSIGNALED(status));
      }
      /* give me a moment */
      sleep(MAMA_PAUSE);
    } // you can never leave
    /* leave */
    pthread_exit((void *)status);
}

static void mama_process_conf_file(char *conf)
{
  FILE *fp = NULL;
  int i = 0;
  int j = 0;
  int rc = 1;
  int rc2 = 0;
  int len = 0;
  char * tmp = NULL;
  char * string = NULL;
  char ** argv = NULL;
  pthread_t tid = 0;

  /* opening file for reading (setenv MAMA=/www/apachedft/conf/mama.conf) */
  fp = fopen(getenv("MAMA"), "r");
  while (rc > 0) {
    /* new argv for each application */
    string = malloc(MAMA_MAX_ARGV*MAMA_MAX_STR);
    memset(string,0,sizeof(MAMA_MAX_ARGV*MAMA_MAX_STR));
    argv = malloc(MAMA_MAX_ARGV*sizeof(string));
    memset(argv,0,MAMA_MAX_ARGV*sizeof(string));
    for (i=0, j=0, tmp = string; rc > 0 && i<MAMA_MAX_ARGV; i++) {
      /* read file line */
      rc = fgets(tmp, MAMA_MAX_STR, fp);
      if (rc > 0) {
        /* # comment */
        if (*tmp == '#') {
          continue;
        }
        /* KEY=value */
        len = strlen(tmp);
        argv[j] = tmp;
        tmp += len;
        while(tmp > argv[j] && (!*tmp || *tmp == 0x0a)) {
          *tmp = '\0';
          tmp -= 1;
        }
        tmp = argv[j];
        /* collected all, execute */
        if (strstr(tmp,"EXECUTE=")) {
          rc2 = pthread_create(&tid, NULL, mama_start, (void *)argv);
          break;
        }
        len = strlen(tmp);
        tmp += len + 1;
        j++;
      }
    }
  }
  fclose(fp);
}

void main(void)
{
  int retcode = 0, szContent=0, rTot = 0, rSz = 0;
  char * req = (char *) NULL;
  char * get = (char *) NULL;
  char * pContent = NULL;
  char getbuf[512000];
  char buff[512000];

  /* start up any commands (setenv MAMA=/www/apachedft/conf/mama.conf) */
  mama_process_conf_file(getenv("MAMA"));

  /* wait in fastcgi for http:://myibmi/any.mama */
  while (FCGI_Accept() >= 0)   {
    req = getenv("REQUEST_METHOD");
    switch (req[0]) {
    case 'P':
      switch (req[1]) {
      case 'O':
        get = getenv("CONTENT_LENGTH");
        szContent = atoi(get);
        // -----
        // read from stdin (Apache)
        pContent = (char *) &getbuf;
        memset(getbuf,0,sizeof(getbuf));
        fread(getbuf, szContent, 1, stdin);
        sprintf(buff,"REQUEST_METHOD %s ok.",req);
        mama_http_200(buff);
        continue;
        break;
      default:
        break;
      }
      break;
    case 'G':
      get = getenv("QUERY_STRING");
      sprintf(buff,"REQUEST_METHOD %s ok.",req);
      mama_http_200(buff);
      continue;
      break;
    default:
      break;
    }
    /* error */
    sprintf(buff,"REQUEST_METHOD %s is unsupported.",req);
    mama_http_403(buff);
    /* debug */
    /* sleep(300); */
  } /* while */
}
