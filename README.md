![mama](http://download.litmis.com.s3.amazonaws.com/cdn/mama_logo.png)
# mama

Mama is a fastcgi start/watch standalone web server applications (node, python, etc.).
The tool enables fastcgi integration of stand-alone web servers into existing Apache sites.

Why?
I needed a simple Apache easily integrated tool to start/watch node examples on Yips site.
I do not own Yips machine, therefore administrative actions resulting in Apache restart are essentially 'random'.  
This tool is only 'starter' for more advanced start/watch/stop. This does trick for minimum tool for my usage.

## pre-compiled test version mama
* [YIPS mama](http://yips.idevcloud.com/wiki/index.php/FastCGI/Mama)

ftp mama
```
unzip mama-n.n.zip
ftp myibmi
> bin
> cd /QOpenSys/usr/bin
> put mama
```

## example configuration (included source)
* conf_ut28p63 - more secure (authentication,  xmlservice only 127.0.0.1) - configuration for Apache instance apachedft lab required profile machine (see source/conf_ut28p63/README.md)
* conf_kt4001 - average secure (xmlservice only 127.0.0.1 ) - configuration for Apache instance apachedft on litmis machine (see source/conf_kt4001/README.md)
* conf_yips -  open (full access) - configuration for Apache instance zendsvr6 on yips education machine (see source/conf_yips/README.md)

## nodejs samples on yips using mama
* [YIPS nodejs sample applications](http://yips.idevcloud.com/wiki/index.php/NodeJs/NodeJs) (see better samples)
* [YIPS xmlservice *NONE uid/pwd](http://yips.idevcloud.com/wiki/index.php/Xmlservice/XMLSERVICEInstall) (see XMLSERVICE *NONE uid/pwd)

## working (3 node applications this example)
```
        APACHEDFT    QTMHHTTP    BCI	  .0  PGM-zfcgi        SELW
        APACHEDFT    QTMHHTTP    BCI      .0  PGM-mama         TIMW
        APACHEDFT    QTMHHTTP    BCI	  .0  PGM-node         SELW
        APACHEDFT    QTMHHTTP    BCI	  .0  PGM-node         SELW
        APACHEDFT    QTMHHTTP    BCI	  .0  PGM-node         SELW
```
Note: If mama can not start your application (node above), she will retry every ten seconds.
You can usually find application problems in Apache logs (see problems). 

## technical
Mama creates one 'monitor' thread per application start (START=app ... EXECUTE=app). 
The new thread forks a child process (new job), processes setenv, etc, from mama.conf for 'START=application', then execs application (node example).
Meanwhile parent thread goes into a waitpid for the child pid. If parent thread detects process exit status, will restart the child process application.
The parent thread pauses (sleep) for 10 seconds between re-starts to avoid flooding the Apache log. At moment, no max try is implemented (check log and wrkactjob mate).

## problems.
* mama - if you kill one of mama's child jobs (node job), she will wait ten seconds and try restart the application. 
Mama ending children is critical for 'integrated' Apache restart. Apache restart mama and children function was only minimal tested,
but seemed to work. There is no persistent file for mama check of 'live' children, so, do not hand start node applications,
they are suppose to live and die with mama. Only problem i found was occasional TCP port getting stuck 'open' (below).  
* NodeJS - console function of node does not work under mama. You should remove or silence console.log('message'). 
I did not debug, most likely an issue due to web using stdin, stdout, stderr, therefore console.log has no where to go (stream failure).
* NodeJS - You should find application 'dump' errors in Apache log.  In example below port 47710 was already in use,  
which i killed the job using 47710 using wrktcpsts, 3. Work with IPv4 connection status, 4   47710  036:47:55  Listen (47710 was mine). 
```
bash-3.00$ cd /www/zendsvr6/logs/
bash-3.00$ cat error_log.Q117073100

Error: listen EADDRINUSE :::47710
    at Object.exports._errnoException (util.js:1026:11)
    at exports._exceptionWithHostPort (util.js:1049:20)
    at Server._listen2 (net.js:1257:14)
    at listen (net.js:1293:10)
    at Server.listen (net.js:1389:5)
    at Function.app.listen (/home/ADC/node4_tutorial_bears/node_modules/express/
```
